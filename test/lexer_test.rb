require 'gundoochy'
require 'token/token'
require 'lexer/lexer'
require 'minitest'

class LexerTest < Minitest::Test
  
  def setup
    @lexy = Lexer.new('five = 5; 
                       ten = 10;')
  end


  def test_next_token
     
     tests = [
       Token.new(IDENT , "five"),
       Token.new(ASSIGN , "="), 
       Token.new(INT , "5"),
       Token.new(SEMICOLON , ";"),
       Token.new(IDENT , "ten"),
       Token.new(ASSIGN , "="), 
       Token.new(INT , "10"),
       Token.new(SEMICOLON , ";"),
       #Token.new(IDENT , "add"),
       #Token.new(ASSIGN , "="),
       #Token.new(FUNCTION , "def"),
       #Token.new(LPAREN , "("),
       #Token.new(IDENT , "x"),
       #Token.new(COMMA , ","),
       #Token.new(IDENT , "y"),
       #Token.new(RPAREN , ")"),
       #Token.new(LCURLY , "{"),
       #Token.new(IDENT , "x"),
       #Token.new(PLUS , "+"),
       #Token.new(IDENT , "y"),
       #Token.new(RCURLY , "}"),
       #Token.new(SEMICOLON , ";"),
       #Token.new(IDENT , "result"),
       #Token.new(ASSIGN , "="),
       #Token.new(IDENT , "add"),
       #Token.new(LPAREN , "("),
       #Token.new(IDENT , "five"),
       #Token.new(COMMA , ","),
       #Token.new(IDENT , "sten"),
       #Token.new(RPAREN , ")"),
       #Token.new(SEMICOLON , ";"),
       #Token.new(EOF , ""),

       #Token.new(PLUS , "+"),
       #Token.new(MINUS , "-"), 
       #Token.new(DIVIDE , "/"), 
       #Token.new(MULTIPLY , "*"), 
       #Token.new(MODULO , "%"), 
       #Token.new(EQUAL_TO , "=="),
       #Token.new(GREATER_THAN , ">"), 
       #Token.new(LESS_THAN , "<"), 
       #Token.new(UNEQUAL_TO , "!="),
       #Token.new(AND , "&&"),
       #Token.new(OR , "||"),
       #Token.new(NOT , "!"),
       #Token.new(COMMA , ","), 
       #Token.new(SEMICOLON , ";"), 
       #Token.new(QUESTION , "?"), 
       #Token.new(LPAREN , "("), 
       #Token.new(RPAREN , ")"), 
       #Token.new(LSQUARE , "["), 
       #Token.new(RSQUARE , "]"), 
       #Token.new(LCURLY , "{"), 
       #Token.new(RCURLY , "}"  
     ]
      
     
     tests.each do |expected_type, expected_literal|
        
        tok = @lexy.next_token
        puts "moimoi",  expected_type, tok.type, expected_literal, tok.literal

        assert_equal expected_type, tok.type
        
        #puts tok.type, expected_type, tok.literal, expected_literal 
        
        assert_equal expected_literal, tok.literal
     end

  end 
end
